const ID_SPLITTER = '_';

$( window ).load(
    function() {
        var url = location.href;

        var logs = $( '#logs' );

        setEventHandlerToInviteButton(
            url
            , logs
        );

        setEventHandlerToDeleteButton(
            url
            , logs
        );
    }
);

function setEventHandlerToInviteButton(
    _url
    , _logs
)
{
    var inviteButton = $( '#invite' );
    var inviteEmail = $( '#inviteEmail' );

    var notYetEmails = $( '#notYetEmails' );

    inviteButton.click(
        function() {
            invite(
                _url
                , _logs
                , inviteEmail
                , notYetEmails
            );
        }
    );
}

function setEventHandlerToDeleteButton(
    _url
    , _logs
)
{
    var deleteButton = $( '#delete' );

    deleteButton.click(
        function() {
            deleteInvite(
                _url
                , _logs
            );
        }
    );
}

function invite(
    _url
    , _logs
    , _email
    , _notYetEmails
)
{
    var email = _email.val();
    if( email.length <= 0 ) {
        appendErrorLog(
            _logs
            , '招待するメールアドレスが入力されていません'
        );

        return;
    }

    appendInfoLog(
        _logs
        , '"' + email + '"を招待中'
    );

    $.ajax(
        _url
        , {
            type : 'POST',
            data : {
                email : email,
            },
            success : function() {
                inviteSuccess(
                    _logs
                    , _notYetEmails
                    , email
                );
            },
            error : function() {
                inviteError(
                    _logs
                    , email
                );
            },
        }
    );
}

function inviteSuccess(
    _logs
    , _notYetEmails
    , _email
)
{
    appendNotYetEmail(
        _notYetEmails
        , _email
    );

    appendSuccessLog(
        _logs
        , '"' + _email + '"の招待に成功しました'
    );
}

function inviteError(
    _logs
    , _email
)
{
    appendErrorLog(
        _logs
        , '"' + _email + '"の招待に失敗しました'
    );
}

function deleteInvite(
    _url
    , _logs
)
{
    var checkboxes = $( 'input[type="checkbox"]' ).filter( ':checked' );

    var length = checkboxes.length;

    if( length <= 0 ) {
        appendErrorLog(
            _logs
            , '削除するメールアドレスが1つもチェックされていません'
        );

        return;
    }

    for( var i = 0 ; i < length ; i++ ) {
        deleteInviteMain(
            _url
            , _logs
            , checkboxes[ i ]
        );
    }
}

function deleteInviteMain(
    _url
    , _logs
    , _checkbox
)
{
    var email = idToEmail(
        _checkbox.id
    );

    appendInfoLog(
        _logs
        , '"' + email + '"の招待を削除中'
    );

    $.ajax(
        _url
        , {
            type : 'DELETE',
            data : {
                email : email,
            },
            success : function() {
                deleteInviteSuccess(
                    _logs
                    , _checkbox
                    , email
                );
            },
            error : function(
                _xhr
            )
            {
                deleteInviteError(
                    _logs
                    , _checkbox
                    , email
                    , _xhr.status
                );
            },
        }
    );
}

function deleteInviteSuccess(
    _logs
    , _checkbox
    , _email
)
{
    removeNotYetEmailByCheckbox(
        _checkbox
    );

    appendSuccessLog(
        _logs
        , '"' + _email + '"の招待の削除に成功しました'
    );
}

function deleteInviteError(
    _logs
    , _checkbox
    , _email
    , _status
)
{
    switch( _status ) {
    case 404:
        removeNotYetEmailByCheckbox(
            _checkbox
        );
        appendErrorLog(
            _logs
            , '"' + _email + '"の招待が存在しませんでした'
        );
        break;

    default:
        appendErrorLog(
            _logs
            , '"' + _email + '"の招待の削除に失敗しました'
        );
        break;
    }
}

function appendNotYetEmail(
    _notYetEmails
    , _email
)
{
    var id = emailToId(
        _email
    );

    removeNotYetEmailById(
        id
    );

    var newEmail = $( '<div />' );
    var checkbox = $( '<input type="checkbox" id="' + id + '" />' );
    newEmail.append( checkbox );
    newEmail.append( $( '<label for="' + id + '">' + _email + '</label>' ) );

    _notYetEmails.append( newEmail );
}

function removeNotYetEmailById(
    _id
)
{
    var checkBox = $( '#' + _id );

    if( checkBox.length > 0 ) {
        removeNotYetEmail(
            checkBox
        );
    }
}

function removeNotYetEmailByCheckbox(
    _checkbox
)
{
    removeNotYetEmail(
        $( _checkbox )
    );
}

function removeNotYetEmail(
    _checkbox
)
{
    _checkbox.parent().remove()
}

function emailToId(
    _email
)
{
    var length = _email.length;

    var charCodes = new Array( length );

    for( var i = 0 ; i < length ; i++ ) {
        charCodes[ i ] = _email.charCodeAt( i );
    }

    return charCodes.join( ID_SPLITTER );
}

function idToEmail(
    _id
)
{
    var charCodes = _id.split( ID_SPLITTER );

    var length = charCodes.length;

    var email = '';

    for( var i = 0 ; i < length ; i++ ) {
        email += String.fromCharCode( charCodes[ i ] );
    }

    return email;
}

function appendInfoLog(
    _logs
    , _log
)
{
    appendLog(
        _logs
        , _log
        , 'logInfo'
    );
}

function appendSuccessLog(
    _logs
    , _log
)
{
    appendLog(
        _logs
        , _log
        , 'logSuccess'
    );
}

function appendErrorLog(
    _logs
    , _log
)
{
    appendLog(
        _logs
        , _log
        , 'logError'
    );
}

function appendLog(
    _logs
    , _log
    , _class
)
{
    var date = generateDate();

    var element = $( '<div>[' + date + '] ' + _log + '</div>' );
    element.addClass( _class );

    _logs.prepend( element );
}

function generateDate(
)
{
    var date = new Date();

    return date.getFullYear()
        + '/' +
        getDigitsValue(
            date.getMonth() + 1
            , 2
        )
        + '/' +
        getDigitsValue(
            date.getDate()
            , 2
        )
        + ' ' +
        getDigitsValue(
            date.getHours()
            , 2
        )
        + ':' +
        getDigitsValue(
            date.getMinutes()
            , 2
        )
        + ':' +
        getDigitsValue(
            date.getSeconds()
            , 2
        )
        + '.' +
        getDigitsValue(
            date.getMilliseconds()
            , 3
        )
    ;
}

function getDigitsValue(
    _value
    , _digits
)
{
    var padding = new Array( _digits ).join( '0' );

    return ( padding + _value ).slice( -_digits );
}
