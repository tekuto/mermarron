package util

import (
    "net/http"
)

type Handler func(
    *http.ResponseWriter,
    *http.Request,
)

func GenerateHandler(
    _getHandler func(
        *http.Request,
    ) Handler,
) func(
    http.ResponseWriter,
    *http.Request,
) {
    return func(
        _response   http.ResponseWriter,
        _request    *http.Request,
    ) {
        if handler := _getHandler(
            _request,
        ) ; handler == nil {
            _response.WriteHeader( 405 )
        } else {
            handler(
                &_response,
                _request,
            )
        }
    }
}
