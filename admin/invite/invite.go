package invite

import (
    "net/http"
    "admin/invite/get"  //FIXME "./get"
    "admin/invite/post" //FIXME "./post"
    "admin/invite/delete"   //FIXME "./delete"
    "util"
)

func init(
) {
    http.HandleFunc(
        "/admin/invite",
        util.GenerateHandler(
            getHandler,
        ),
    )
}

func getHandler(
    _request    *http.Request,
) util.Handler {
    switch _request.Method {
    case "GET":
        return get.Handler

    case "POST":
        return post.Handler

    case "DELETE":
        return delete.Handler
    }

    return nil
}
