package get

import (
    "net/http"
    "fmt"
)

func Handler(
    _response   *http.ResponseWriter,
    _request    *http.Request,
) {
    fmt.Fprint(
        *_response,
`<html>
    <head>
        <title>invite</title>
        <link rel="stylesheet" type="text/css" href="invite.css" />
        <script type="text/javascript" src="/jquery-2.0.2.min.js"></script>
        <script type="text/javascript" src="invite.js"></script>
    </head>
    <body>
        <div>
            <p>メールアドレス: <input type="text" id="inviteEmail" /><input type="button" id="invite" value="招待" /><span id="inviteMessage"></span></p>
        </div>
        <hr />
        <div>
            <p>入会待ちメールアドレス</p>
            <div id="notYetEmails">
                <!-- TODO -->
            </div>
            <p>
                <input type="button" id="delete" value="削除" />
            </p>
        </div>
        <hr />
        <p>ログ</p>
        <div id="logs" />
    </body>
</html>`,
    )
}
